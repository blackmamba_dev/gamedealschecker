### How to install? ###
* git clone the repo
* add these dependencies to project
* * Sqlite JDBC driver (https://github.com/xerial/sqlite-jdbc/releases)
* * Selenium driver - make sure you import every library (https://www.selenium.dev/downloads/)
* * JDA - (https://github.com/DV8FromTheWorld/JDA/releases)
* add these variables to system environment variables
* * ChromeWebDriver (Path where chromewebdriver is located. Example: C:\Java\chromedriver.exe)
* * DiscordBotToken - (Token for discord bot api)
* * DiscordChannelToken - (Channel token where you want to post new deals)
* * GameDeals_SQLiteDB - (Path where you want to have your sqlite db. Example:C:\Java\db\ )
