package com.gamedealschecker;

import java.sql.*;

public class Datasource {
    public static final String DB_NAME = "gameDeals.db";
    private static String DB_FILE_LOCATION = System.getenv("GameDeals_SQLiteDB");

    public static final String CONNECTION_STRING = "jdbc:sqlite:" + DB_FILE_LOCATION + DB_NAME;
    public static final String TABLE_NAME = "deals";

    public static final String DEALS_ID = "_id";
    public static final String DEALS_TITLE = "title";
    public static final String DEALS_URL = "url";
    public static final String DEALS_DATE = "date";

    public static final String QUERY_GAMEDEAL = "SELECT " + DEALS_TITLE + "," + DEALS_URL + "," + DEALS_DATE + "," + DEALS_ID + " FROM " + TABLE_NAME + " WHERE " + DEALS_TITLE + " = ?";
    public static final String INSERT_GAMEDEALS = "INSERT INTO " + TABLE_NAME + "(" + DEALS_TITLE + "," + DEALS_URL + "," + DEALS_DATE + ") VALUES (?, ?, ?)";

    private Connection conn;
    private PreparedStatement insertGameDeal;
    private PreparedStatement queryGameDeal;

    public boolean open() {
        try {
            conn = DriverManager.getConnection(CONNECTION_STRING);
            Statement statement = conn.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + DEALS_ID + " INTEGER NOT NULL PRIMARY KEY, " + DEALS_TITLE + " TEXT ," + DEALS_URL + " TEXT, " + DEALS_DATE + " TEXT)");
            insertGameDeal = conn.prepareStatement(INSERT_GAMEDEALS, Statement.RETURN_GENERATED_KEYS);
            queryGameDeal = conn.prepareStatement(QUERY_GAMEDEAL);

            return true;
        } catch (SQLException e) {
            System.out.println("Couldn't connect to database: " + e.getMessage());
            return false;
        }
    }

    public void close() {
        try {
            if(insertGameDeal != null) {
                insertGameDeal.close();
                System.out.println("DB Insert connection closed");
            }
            if(queryGameDeal != null) {
                queryGameDeal.close();
                System.out.println("DB Query connection closed");
            }
            if (conn != null) {
                conn.close();
                System.out.println("DB exited");
            }
        } catch (SQLException e) {
            System.out.println("Couldn't close connection: " + e.getMessage());
        }
    }

    public int insertGameDeal(String title, String URL) throws SQLException {
        queryGameDeal.setString(1, title);
        ResultSet results = queryGameDeal.executeQuery();
        java.util.Date utilDate = new java.util.Date();

        if(results.next()) {
            return results.getInt(1);
        } else {
            System.out.println("Game " + title + " has been added to db at time : " + utilDate);
            insertGameDeal.setString(1, title);
            insertGameDeal.setString(2, URL);
            insertGameDeal.setObject(3, utilDate);
            int affectedRows = insertGameDeal.executeUpdate();

            if(affectedRows != 1) {
                throw new SQLException("Couldn't insert game deal!");
            } else {
                return -1;
            }
            /*ResultSet generatedKeys = insertGameDeal.getGeneratedKeys();
            if(generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                throw new SQLException("Couldn't get _id for game deal");
            }*/
        }
    }
}
