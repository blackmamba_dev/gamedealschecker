package com.gamedealschecker;

import org.openqa.selenium.WebElement;

import java.sql.SQLException;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Datasource datasource = new Datasource();
        Discord discord = new Discord();

        if(discord.connectToBot()) {
            System.out.println("Discord bot online");
        } else {
            System.out.println("I can't connect to a bot");
        }
        if(!datasource.open()) {
            System.out.println("Can't open datasource");
            return;
        } else {
            System.out.println("Connected to datasource");
        }
        HtmlParser htmlparser = new HtmlParser();
        if(htmlparser.open_parser()) {
            List<WebElement> gamedealWebElement = htmlparser.checkForNewDeals();
            for(WebElement deal : gamedealWebElement) {

                try {
                    if(deal.getText().contains("FREE on Steam") || deal.getText().contains("FREE on Epic Games")) {
                        if( datasource.insertGameDeal(deal.getText(),deal.getAttribute("href")) == -1 ) {
                            System.out.println("Game deal not in db");
                            discord.sendDealToDChannel(deal.getText(),deal.getAttribute("href"));
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("Game deal is already in db");
                        }
                    }
                } catch (SQLException e) {
                    System.out.println("Something went wrong");
                    e.printStackTrace();
                }
            }
        }
        htmlparser.close_parser();
        datasource.close();
        discord.closeDiscordBot();
        System.exit(0);
    }

}
