package com.gamedealschecker;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.TextChannel;

import javax.security.auth.login.LoginException;

public class Discord {
    JDA jda;
    private final String DiscordBotToken = System.getenv("DiscordBotToken");
    private final String DiscordChannelToken = System.getenv("DiscordChannelToken");

    public boolean connectToBot() {
        try {
            jda = new JDABuilder(DiscordBotToken).setActivity(Activity.watching("for new games")).build();
            return true;
        } catch (LoginException e) {
            e.printStackTrace();
            return false;
        }
    }
    public void closeDiscordBot() {
        try {
            jda.shutdown();
            System.out.println("Discord bot shutdown");
            jda.shutdownNow();
            System.out.println("Discord bot shutdownNow");
        } catch (Exception e) {
            System.out.println("Error closing discord bot " + e.getMessage());
        }
    }
    public void sendDealToDChannel(String title, String URL) {
        TextChannel channel = jda.getTextChannelById(DiscordChannelToken);
        if(channel != null) {
            channel.sendMessage(title + " " + URL).queue();
        } else {
            System.out.println("Channel not found");
        }
    }
}
