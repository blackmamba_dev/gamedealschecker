package com.gamedealschecker;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.List;

public class HtmlParser {

    private WebDriver driver;
    private final String chromeWebDriver = System.getenv("ChromeWebDriver");
    String systemType = System.getProperty("os.name").toLowerCase();

    public boolean open_parser() {
        try {
            System.setProperty("webdriver.chrome.driver", chromeWebDriver);
            return true;
        } catch (Exception e) {
            System.out.println("Something with opening chrome parser went wrong: " + e.getMessage());
            return false;
        }
    }

    public List<WebElement> checkForNewDeals() {
        //Set Chrome Headless mode as TRUE
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("disable-infobars");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");
        //Instantiate Web Driver
        WebDriver driver = new ChromeDriver(options);

        driver.get("https://isthereanydeal.com/specials/#/filter:&giveaway");
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return driver.findElements(By.cssSelector(".bundle-title>a")); //return an empty list if elements not found
    }

    public void close_parser() {
        try {
            driver.close();
            driver.quit();

        } catch (Exception e) {
            e.getStackTrace();
        }
        if (systemType.contains("win")) {
            try {
                // Selenium drivers don't always close properly, kill them
                System.out.println("Close one or more driver exe files");
                Runtime.getRuntime().exec("taskkill /f /im chromedriver.exe");
                Runtime.getRuntime().exec("taskkill /f /im chrome.exe");
            } catch (IOException e) {
                System.out.println("Failed to close one or more driver exe files");
            }
        } else {
            try {
                Runtime.getRuntime().exec("pkill chromedriver");
                Runtime.getRuntime().exec("pkill chrome");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Webdriver & Chrome quited");

    }
}
